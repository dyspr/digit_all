var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var font
var steps = 9
var array = create2DArray(1, steps, 0, false)
var numbers = '0123456789'

function preload() {
  font = loadFont('ttf/RobotoMono-Medium.ttf')
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  textFont(font)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < steps; i++) {
    push()
    translate(windowWidth * 0.5 + boardSize * 0.15 * sin(frameCount * 0.01 + i * 0.25), windowHeight * 0.5 - boardSize * (1 - (i / steps)) * 0.175)
    textSize(boardSize * (1 - (i / steps)))
    textAlign(CENTER, CENTER)
    fill(255 * ((i + 1) % 2))
    stroke(255 * ((i + 1) % 2))
    strokeWeight((i / steps) * boardSize * 0.01)
    text(numbers[array[0][i]], 0, 0)
    pop()
  }

  array[0][Math.floor(Math.random() * steps)] = array[0][Math.floor(Math.random() * steps)]
  if (frameCount % 3 === 0) {
    array[0][Math.floor(Math.random() * steps)] = numbers[Math.floor(Math.random() * numbers.length)]
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.floor(Math.random() * 10)
      }
    }
    array[i] = columns
  }
  return array
}
